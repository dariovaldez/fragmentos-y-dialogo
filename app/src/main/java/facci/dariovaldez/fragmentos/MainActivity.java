package facci.dariovaldez.fragmentos;

import android.app.Dialog;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, frmUno.OnFragmentInteractionListener, FrmDos.OnFragmentInteractionListener {

    Button btnFrm1, btnFrm2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnFrm1 = (Button) findViewById(R.id.btnFrag1);
        btnFrm2 = (Button) findViewById(R.id.btnFrag2);

        btnFrm1.setOnClickListener(this);
        btnFrm2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnFrag1:
                frmUno fragmentoUno = new frmUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;

            case R.id.btnFrag2:
                FrmDos fragmentoDos = new FrmDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);


                Button btnAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText txtUser = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText txtPass = (EditText) dialogoLogin.findViewById(R.id.txtPassword);

                dialogoLogin.show();

                btnAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "Usuario: "+ txtUser.getText().toString() +
                                        "\n Pass: " + txtPass.getText().toString() , Toast.LENGTH_SHORT).show();
                    }
                });

                break;

            case R.id.opcionRegistro:

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
